const	config_db = require('../src/mongodb/config_db.js'),
  db = require('../src/mongodb/connect_db.js');

function read_one(
	collection,
	filter,
	projection
) {
	return db.get().collection(collection).findOne(filter, projection);
}


read_one(config_db.collection.USER, { PSID: 2369292769772575, IS_ACTIVE: 1 }, {});

console.log(read_one(config_db.collection.USER, { PSID: 2369292769772575, IS_ACTIVE: 1 }, {}));

