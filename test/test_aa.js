async function quanh(a) {
  if (a >= 0) {
    return true;
  } else {
    return false;
  }
}

var linh = quanh(1).then(a => {
  console.log(a);
})

const w = {
  coord: { lon: 105.85, lat: 21.03 },
  weather:
    [{
      id: 803,
      main: 'Clouds',
      description: 'broken clouds',
      icon: '04d'
    }],
  base: 'stations',
  main:
  {
    temp: 31,
    pressure: 1000,
    humidity: 66,
    temp_max: 31
  },
  visibility: 10000,
  wind: { speed: 7.2, deg: 30 },
  clouds: { all: 75 },
  dt: 1562666028,
  sys:
  {
    type: 1,
    id: 9308,
    message: 0.0055,
    country: 'VN',
    sunrise: 1562624465,
    sunset: 1562672516
  },
  timezone: 25200,
  id: 1581130,
  name: 'Hanoi',
  cod: 200
}