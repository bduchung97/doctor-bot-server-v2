const result = ['Không có bệnh', 'Có dấu hiệu của bệnh tim', 'Có bệnh'];

exports.kiem_tra_benh = function kiem_tra_benh(age,
	gender,
	cp,
	trestbps,
	fbs,
	exang,
	oldpeak,
	slope,
	thal) {
	var rs;
	if (age < 60) {
		if (oldpeak >= 0.4) {
			rs = result[1];
			if (slope == 2) {
				rs = result[2];
			} else if (thal == 7) {
				rs = result[2];
			}
			return rs;
		} else {
			if (fbs == 0) {
				rs = result[0];
				if (thal == 7) {
					rs = result[1];
				} else if (cp == 2 || cp == 4) {
					rs = result[1];
				} else if (thal == 7 && slope == 1) {
					rs = result[2];
				} else if (thal == 3 && thalac > 33 && slope == 1) {
					rs = result[2];
				}
			} else {
				rs = result[1];
			}
		}
	} else {
		rs = result[1];
		if(trestbps > 129){
			if(exang == 0){
				rs = result[1];
			} else{
				rs = result[2];
			}
		} else {
			if(cp == 1 || cp == 3){
				rs = result[2];
			}
		}
	}
	return rs;
}