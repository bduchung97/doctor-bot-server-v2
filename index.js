const
  express = require('express'),
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  fileUpload = require('express-fileupload'),
  app = express().use(bodyParser.json()),
  db = require('./src/mongodb/connect_db'),
  ga = require('./src/mongodb/gen_data'),
  PORT = process.env.PORT || 1337;

app.use(cookieParser());
app.use(function (_req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers',
    'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(fileUpload());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})) // support encoded bodies

db.connect().then(() => {
  ga.gen_data().then(() => {
    app.listen(PORT, () => console.log('ICA is listening on PORT: ' + PORT))
  }).catch(err => {
    console.log(err)
    console.log('Cant generate data!')
  })
}).catch(err => {
  console.log(err)
  console.log('Cant connect to DB!')
})

// Facebook
// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {
  console.log('/webhook');
  let pw = require('./src/services/facebook/post_webhook');
  pw.post_webhook(req, res);
});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {
  console.log('/webhook');
  let gw = require('./src/services/facebook/get_webhook');
  gw.get_webhook(req, res);
});

app.post('/post_diagnose', (req, res) => {
  console.log('/post_diagnose');
  let pd = require('./src/services/diagnose_plugin/post_diagnose');
  pd.post_diagnose(req, res);
})

app.post('/post_kiem_tra_benh', (req, res) => {
  console.log('post_kiem_tra_benh');
  let pktb = require('./src/services/kiemtrabenh_plugin/post_kiem_tra_benh.js');
  pktb.post_kiem_tra_benh(req, res);
})

app.post('/post_create_appointment_bachmai', (req, res) => {
  console.log('post_create_appointment_bachmai');
  let pca = require('./src/services/create_appointment/post_create_appointment_bachmai.js');
  pca.post_create_appointment_bachmai(req, res);
})
