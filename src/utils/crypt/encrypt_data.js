let CryptoJS = require("crypto-js");

exports.encrypt_data = function encrypt_data(data) {
  if (typeof data == 'string') {
    let key = process.env.MASTER_KEY || 'doctor-bot';
    return CryptoJS.AES.encrypt(data, key).toString();
  } else {
    return null;
  }
}

// console.log(encrypt_data('2369292769772575'));

