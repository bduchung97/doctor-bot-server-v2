var md5 = require('md5');

exports.checksum = function checksum(data) {
  if (typeof data == 'string') {
    return md5(data);
  } else {
    return null;
  }
}
