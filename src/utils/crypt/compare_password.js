const bcrypt = require('bcryptjs');

exports.compare_password = function compare_password(password_1, password_2) {
  if (bcrypt.compareSync(password_1, password_2) == true) {
    return true;
  }
  else {
    return false;
  }
}
