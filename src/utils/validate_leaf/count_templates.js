const db = require('../../mongodb/connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../../mongodb/config_db');

exports.count_templates = async function count_templates(node_id) {
  return await db.get().collection(config.collection.TEMPLATE).countDocuments({ NODE__ID: ObjectId(node_id) });
}
