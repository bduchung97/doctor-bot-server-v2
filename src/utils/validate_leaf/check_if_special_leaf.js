exports.check_if_special_leaf = async function check_if_special_leaf(node_id) {
  let special_function_ID = ['GET_STARTED', 'HOME', 'DEFAULT', 'PERSISTENT_MENU'],
    son = require('../../mongodb/node/show_one_node'),
    node_data = await son.show_one_node(node_id);
  if (special_function_ID.indexOf(node_data.FUNCTION_ID) !== -1) {
    return true;
  } else {
    return false;
  }
}
