const nodejsWeatherApp = require('nodejs-weather-app');

exports.fetch_weather_forecast = async function fetch_weather_forecast() {
  try {
    return await nodejsWeatherApp.getWeather('Hanoi');
  } catch (error) {
    console.error(error);
  }
}
