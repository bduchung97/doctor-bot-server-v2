let fwf = require('./fetch_weather_forecast.js');

exports.handle_weather_payload = async function handle_weather_payload() {
  let weather_data = await fwf.fetch_weather_forecast(),
    location = weather_data.name,
    temperature = weather_data.main.temp,
    humidity = weather_data.main.humidity,
    main = weather_data.weather[0].main,
    sol = require('../../mongodb/leaf/show_one_leaf.js'),
    weather_leaf = await sol.show_one_leaf('WEATHER');
  weather_leaf[0].VALUE.attachment.payload.elements[0].subtitle = location + "\nTemperature: " + temperature + "°C - " + main + "\nHumidity: " + humidity + "%";
  return weather_leaf[0].VALUE;
}
