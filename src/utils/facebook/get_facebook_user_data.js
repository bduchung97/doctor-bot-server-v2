const mr = require('../request/make_request.js'),
  PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || 'EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD';

exports.get_fb_user_info = async function get_fb_user_info(PSID) {
  let uri = 'https://graph.facebook.com/' + PSID.toString() + '?fields=first_name,last_name,profile_pic,name&access_token=' + PAGE_ACCESS_TOKEN,
    user_fb_info = await mr.make_request(uri, {}),
    result = JSON.parse(user_fb_info);
    
  return result;
}

// let a = async()=> {
// 	console.log(await get_fb_user_info(2369292769772575))
// }

// a();