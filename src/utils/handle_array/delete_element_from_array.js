exports.delete_element_from_array = function delete_element_from_array(arr, element) {
  let i = arr.indexOf(element);
  if (i != -1) {
    arr.splice(i, 1);
    return arr;
  }
  else return null;
}
