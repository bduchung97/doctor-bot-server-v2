exports.delete_duplicate_element = function delete_duplicate_element(array) {
  return [...new Set(array)];
}