exports.request = function request(data_res, callback) {
  const request = require('request'),
    ai_server_url = process.env.AI_SERVER_URL || "http://139.180.131.10",
    ai_server_port = process.env.AI_SERVER_PORT || ":8082";
  request.post({ url: ai_server_url + ai_server_port, form: { data: data_res } }, function (err, res) {
    if (!err) {
      callback(res.body)
    } else {
      throw err;
    }
  });
}
