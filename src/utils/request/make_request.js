const request = require('request');
exports.make_request = async function make_request(uri, options) {
    return new Promise(function (resolve, reject) {
        request(uri, options, function (error, res) {
            if (!error && res.statusCode == 200) {
                resolve(res.body);
            } else {
                reject(error);
            }
        });
    });
}