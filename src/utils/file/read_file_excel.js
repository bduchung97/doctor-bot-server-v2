const XLSX = require('xlsx');

exports.read_file_excel = function read_file_excel(file) {
  let workbook = XLSX.readFile(file),
    sheet_name_list = workbook.SheetNames,
    data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
  return data;
}
