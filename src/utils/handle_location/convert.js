const NodeGeocoder = require('node-geocoder');
var data = require('./data.json')
const options = {
  provider: 'google',

  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyCz6B_LDt8wWk53X4lHEMKP7BozAku-_dM', // for Mapquest, OpenCage, Google Premier
  formatter: 'String'
};

const geocoder = NodeGeocoder(options);

// data.address.map((value) => {
//   geocoder.geocode(value.name)
//   .then((res) => {
//     result = {
//       ...result,
//       res
//     }
//     console.log(result);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
// })
data.coodinates.map((value) => {
  geocoder.reverse({
    lat: value.latitude,
    lon: value.longitude
  }, function (err, res) {
    console.log(res)
  });
})
