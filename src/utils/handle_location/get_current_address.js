const NodeGeocoder = require('node-geocoder');

const options = {
  provider: 'google',

  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyCz6B_LDt8wWk53X4lHEMKP7BozAku-_dM', // for Mapquest, OpenCage, Google Premier
  formatter: 'String'
};

const geocoder = NodeGeocoder(options);


exports.get_current_address = function get_current_address(latitude, longitude, callback) {
  geocoder.reverse({
    lat: latitude,
    lon: longitude
  }, function (err, res) {
    if(res) {
      res.map(i => {
        let formattedAddress = i.formattedAddress
        let neighborhood = i.extra.neighborhood
        let level1long = i.administrativeLevels.level1long
        callback(null, formattedAddress, neighborhood, level1long)
      })
    }
    else {
      callback(null, '54A Nguyễn Chí Thanh, Đống Đa, Hà Nội', 'Đống Đa', 'Hà Nội')
    }
  });
}