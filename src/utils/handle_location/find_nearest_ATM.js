const geolib = require('geolib')

function handleaddress(address) {
  return address.replace(", 100000,", "").replace(", Vietnam", "").replace(", Việt Nam", "").replace(" 100000", "").replace("- Việt Nam", "").replace(" Việt Nam", "").replace(" -", ",").replace(" 10000", "")
}

exports.find_nearest_ATM = function find_nearest_ATM(currentLocation, listATM, callback) {
  let distance_order = geolib.orderByDistance(currentLocation, listATM)
  // 1st
  let nearest_distance = distance_order[0].key
  let coodinates_nearest_distance = distance_order[0]
  let lat1 = coodinates_nearest_distance.latitude
  let lon1 = coodinates_nearest_distance.longitude
  let ATM0 = coodinates_nearest_distance.address

  // 2nd
  let nearest_distance1 = distance_order[1].key
  let ATM1_nearest_distance = distance_order[1]
  let ATM1 = ATM1_nearest_distance.address
  // 3rd
  let nearest_distance2 = distance_order[2].key
  let ATM2_nearest_distance = distance_order[2]
  let ATM2 = ATM2_nearest_distance.address
  // 4th
  let nearest_distance3 = distance_order[3].key
  let ATM3_nearest_distance = distance_order[3]
  let ATM3 = ATM3_nearest_distance.address
  // 5th
  let nearest_distance4 = distance_order[4].key
  let ATM4_nearest_distance = distance_order[4]
  let ATM4 = ATM4_nearest_distance.address
  // 6th
  let nearest_distance5 = distance_order[5].key
  let ATM5_nearest_distance = distance_order[5]
  let ATM5 = ATM5_nearest_distance.address
  callback(null, lat1, lon1, handleaddress(ATM0), handleaddress(ATM1), handleaddress(ATM2), handleaddress(ATM3), handleaddress(ATM4), handleaddress(ATM5))
}