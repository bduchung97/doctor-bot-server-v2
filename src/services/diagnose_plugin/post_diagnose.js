const config_db = require('../../mongodb/config_db.js'),
	uo = require('../../mongodb/commands/update_one.js'),
	ro = require('../../mongodb/commands/read_one');

exports.post_diagnose = async function post_diagnose(req, res) {
	try {
		let PSID = req.body.PSID,
			age = req.body.AGE,
			gender = req.body.GENDER,
			action = req.body.ACTION,
			heart_rate = req.body.HEART_RATE,
			chan_doan = require('../../../test/chan_doan.js');

		if (age && gender && action && heart_rate && PSID) {
			let user_data = await ro.read_one(config_db.collection.USER, {PSID: PSID , IS_ACTIVE: 1}, {});
			if(user_data.GENDER){
				await uo.update_one(config_db.collection.USER, { PSID: PSID, IS_ACTIVE: 1 }, {
					$push: {
						DIAGNOSE: {
							$each: [{
								CREATED_DATE: new Date(),
								STATUS: chan_doan.chan_doan(age, gender, action, heart_rate)
							}]
						}
					}
				})
			} else{
				await uo.update_one(config_db.collection.USER, { PSID: PSID, IS_ACTIVE: 1 },
					{
						$set:{
							GENDER: gender
						}
					})
	
				await uo.update_one(config_db.collection.USER, { PSID: PSID, IS_ACTIVE: 1 }, {
					$push: {
						DIAGNOSE: {
							$each: [{
								CREATED_DATE: new Date(),
								STATUS: chan_doan.chan_doan(age, gender, action, heart_rate)
							}]
						}
					}
				})
			}
			console.log('post_diagnose successfully');
			let csa = require('../facebook/call_send_API.js'),
				sol = require('../../mongodb/leaf/show_one_leaf.js'),
				response = {
					'text': 'Kết quả chẩn đoán của bạn là: ' + chan_doan.chan_doan(age, gender, action, heart_rate)
				},
				default_leaf = await sol.show_one_leaf('DEFAULT');
			await csa.callSendAPI(PSID, response);

			console.log(default_leaf[0].VALUE)
			setTimeout(async () => {
				await csa.callSendAPI(PSID, default_leaf[0].VALUE);
			}, 1000)
			res.status(200).send({ msg_code: '001', data: chan_doan.chan_doan(age, gender, action, heart_rate) });
		} else {
			console.log('Input is invalid!')
			res.status(400).send({ msg_code: '004' });
		}
	} catch (error) {
		console.log(error);
		res.status(500).send({ msg_Code: '500' });
	}
}