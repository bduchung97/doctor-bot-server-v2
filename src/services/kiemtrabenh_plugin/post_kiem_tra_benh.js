const config_db = require('../../mongodb/config_db.js'),
	co = require('../../mongodb/commands/create_one.js');

exports.post_kiem_tra_benh = async function post_kiem_tra_benh(req, res) {
	try {
		let PSID = req.body.PSID,
			age = req.body.AGE,
			gender = req.body.GENDER,
			cp = req.body.CP,
			trestbps = req.body.TRESTBPS,
			fbs = req.body.FBS,
			exang = req.body.EXANG,
			oldpeak = req.body.OLDPEAK,
			slope = req.body.SLOPE,
			thal = req.body.THAL,
			ktb = require('../../../test/kiem_tra_benh.js');

		if (age && gender && PSID) {
			let data = {
				PSID: PSID,
				AGE: age,
				GENDER: gender,
				CP: cp,
				TRESTBPS: trestbps,
				FBS: fbs,
				EXANG: exang,
				OLDPEAK: oldpeak,
				SLOPE: slope,
				THAL: thal,
				CREATED_DATE: new Date(),
				RESULT: ktb.kiem_tra_benh(age,
					gender,
					cp,
					trestbps,
					fbs,
					exang,
					oldpeak,
					slope,
					thal)
			}
			co.create_one(config_db.collection.KQ_BENH, data);

			console.log('post_kiem_tra_benh successfully');
			let csa = require('../facebook/call_send_API.js'),
				sol = require('../../mongodb/leaf/show_one_leaf.js'),
				response = {
					'text': 'Kết quả chẩn đoán của bạn là: ' + ktb.kiem_tra_benh(age,
					gender,
					cp,
					trestbps,
					fbs,
					exang,
					oldpeak,
					slope,
					thal)
				},
				default_leaf = await sol.show_one_leaf('DEFAULT');
			await csa.callSendAPI(PSID, response);

			console.log(default_leaf[0].VALUE)
			setTimeout(async () => {
				await csa.callSendAPI(PSID, default_leaf[0].VALUE);
			}, 1000)
			res.status(200).send({ msg_code: '001', data: ktb.kiem_tra_benh(age,
				gender,
				cp,
				trestbps,
				fbs,
				exang,
				oldpeak,
				slope,
				thal) });
		} else {
			console.log('Input is invalid!')
			res.status(400).send({ msg_code: '004' });
		}
	} catch (error) {
		console.log(error);
		res.status(500).send({ msg_Code: '500' });
	}
}