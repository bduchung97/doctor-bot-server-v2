const call_send_API = require("./call_send_API"),
  sol = require('../../mongodb/leaf/show_one_leaf'),
  cv = require('../../utils/handle_vie/convert_vi'),
  req = require('../../utils/request/request'),
  a = require('./attachment'),
  hwp = require('../../utils/weather/handle_weather_payload'),
  soqrr = require('../../mongodb/quick_replies_ref/show_one_quick_replies_ref'),
  handbook_url = process.env.HANDBOOK_URL || 'https://ica-plugin.herokuapp.com/',
  handbook_port = process.env.HANDBOOK_PORT || '',
  plugin_url = process.env.PLUGIN_URL || 'https://ica-plugin.herokuapp.com/',
  plugin_port = process.env.PLUGIN_PORT || '';

// Handles messages events
exports.handleMessage = async function handleMessage(sender_psid, received_message) {
  let callhome_leaf = await sol.show_one_leaf('CALL_HOME');

  if (received_message.text) {
    let qr = await soqrr.show_one_quick_replies_ref(received_message.text);
    if (qr) {
      let payload = qr.PAYLOAD;
      if (payload == 'WEATHER') {
        weather_leaf = await hwp.handle_weather_payload();
        await call_send_API.callSendAPI(sender_psid, weather_leaf);
        setTimeout(async () => {
          callhome_leaf.map(async (e) => {
            setTimeout(async () => {
              await call_send_API.callSendAPI(sender_psid, e.VALUE);
            }, 1000);
          });
        }, 1000);
      }
      else if (payload == 'HANDBOOK') {
        handbook_leaf = await sol.show_one_leaf(payload);
        handbook_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/handbook";
        handbook_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
        setTimeout(async () => {
          callhome_leaf.map(async (e) => {
            setTimeout(async () => {
              await call_send_API.callSendAPI(sender_psid, e.VALUE);
            }, 1000);
          });
        }, 1000);
      }
      else if (payload == 'HOME') {
        home_leaf = await sol.show_one_leaf('HOME');
        home_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
      }
      else if (payload == 'NETWORK') {
        network_leaf = await sol.show_one_leaf('NETWORK');
        network_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
      }
      else {
        leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
        setTimeout(async () => {
          callhome_leaf.map(async (e) => {
            setTimeout(async () => {
              await call_send_API.callSendAPI(sender_psid, e.VALUE);
            }, 1000);
          });
        }, 1000);
      }
    } else {
      // req.request(cv.change_alias(received_message.text), async function (result) {
      //   var NLP = JSON.parse(result),
      //     payload = NLP.operant,
      //     score = NLP.score;
      //   if (score >= 0.6) {
      //     if (payload == 'WEATHER') {
      //       weather_leaf = await hwp.handle_weather_payload();
      //       await call_send_API.callSendAPI(sender_psid, weather_leaf);
      //       setTimeout(async () => {
      //         callhome_leaf.map(async (e) => {
      //           setTimeout(async () => {
      //             await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //           }, 1000);
      //         });
      //       }, 1000);
      //     }
      //     else if (payload == 'HANDBOOK') {
      //       handbook_leaf = await sol.show_one_leaf(payload);
      //       handbook_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/handbook";
      //       handbook_leaf.map(async (e) => {
      //         setTimeout(async () => {
      //           await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //         }, 1000);
      //       });
      //       setTimeout(async () => {
      //         callhome_leaf.map(async (e) => {
      //           setTimeout(async () => {
      //             await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //           }, 1000);
      //         });
      //       }, 1000);
      //     }
      //     else if (payload == 'LOGIN/REGISTRATION') {
      //       login_leaf = await sol.show_one_leaf(payload);
      //       login_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/login";
      //       login_leaf.map(async (e) => {
      //         setTimeout(async () => {
      //           await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //         }, 1000);
      //       });
      //       setTimeout(async () => {
      //         callhome_leaf.map(async (e) => {
      //           setTimeout(async () => {
      //             await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //           }, 1000);
      //         });
      //       }, 1000);
      //     }
      //     else if (payload == 'HOME') {
      //       home_leaf = await sol.show_one_leaf('HOME');
      //       home_leaf.map(async (e) => {
      //         setTimeout(async () => {
      //           await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //         }, 1000);
      //       });
      //     }
      //     else if (payload == 'NETWORK') {
      //       network_leaf = await sol.show_one_leaf('NETWORK');
      //       network_leaf.map(async (e) => {
      //         setTimeout(async () => {
      //           await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //         }, 1000);
      //       });
      //     }
      //     else {
      //       leaf = await sol.show_one_leaf(payload);
      //       leaf.map(async (e) => {
      //         setTimeout(async () => {
      //           await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //         }, 1000);
      //       });
      //       setTimeout(async () => {
      //         callhome_leaf.map(async (e) => {
      //           setTimeout(async () => {
      //             await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //           }, 1000);
      //         });
      //       }, 1000);
      //     }
      //   }
      //   else {
      //     default_leaf = await sol.show_one_leaf('DEFAULT');
      //     default_leaf.map(async (e) => {
      //       setTimeout(async () => {
      //         await call_send_API.callSendAPI(sender_psid, e.VALUE);
      //       }, 1000);
      //     });
      //   }
      // })
      default_leaf = await sol.show_one_leaf('DEFAULT');
      default_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }
  } else {
    await a.attachment(sender_psid, received_message.attachments[0]);
    callhome_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 4000);
    });
  }
}
