var call_send_API = require('./call_send_API');

exports.video = async function video(sender_psid) {
  response = {
    "text": "You just sent a video."
  }
  await call_send_API.callSendAPI(sender_psid, response);
}