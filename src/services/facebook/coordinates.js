exports.coordinates = async function coordinates(sender_psid, attachment) {
  let attachment_lat = attachment.payload.coordinates.lat;
  let attachment_long = attachment.payload.coordinates.long;
  var gca = require('../../utils/handle_location/get_current_address'),
    call_send_API = require('./call_send_API');
  gca.get_current_address(attachment_lat, attachment_long, async function (err, formattedAddress, neighborhood, level1long) {
    let currentAndress = neighborhood + ', ' + level1long
    responseAndress = {
      "text": "You are in: " + currentAndress,
    }
    await call_send_API.callSendAPI(sender_psid, responseAndress);
    let gNA = require('../../firebase/get_nearests_ATM')
    gNA.get_nearests_ATM(level1long, async function (err, result) {
      let currentLocation = {
        latitude: attachment_lat,
        longitude: attachment_long
      }
      let fNA = require('../../utils/handle_location/find_nearest_ATM')
      fNA.find_nearest_ATM(currentLocation, result, async function (err, lat1, lon1, ATM0, ATM1, ATM2, ATM3, ATM4, ATM5) {
        responseListATM = {
          "text": "Offices near you: \n- " + ATM0 + "\n- " + ATM1 + "\n- " + ATM2 + "\n- " + ATM3 + "\n- " + ATM4 + "\n- " + ATM5,
        }
        await call_send_API.callSendAPI(sender_psid, responseListATM);
        responseDirection = {
          "attachment": {
            "type": "template",
            "payload": {
              "template_type": "generic",
              "elements": [{
                "title": "Directions to the nearest office",
                "image_url": "https://image.viettimes.vn/666x374/Uploaded/2019/ofh_oazszstq/2018_07_18/googlemapsbanner_ftem.jpg",
                "default_action": {
                  "type": "web_url",
                  "url": "https://www.google.com/maps/dir/?api=1&origin=" + attachment_lat + "," + attachment_long + "+&destination=" + lat1 + "," + lon1 + "+&travelmode=walking",
                  "webview_height_ratio": "FULL",
                },
              }]
            }
          },
        }
        await call_send_API.callSendAPI(sender_psid, responseDirection);
      });
    });
  });
}