const call_send_API = require('./call_send_API'),
  sol = require('../../mongodb/leaf/show_one_leaf'),
  hwp = require('../../utils/weather/handle_weather_payload'),
  handbook_url = process.env.HANDBOOK_URL || 'https://ica-plugin.herokuapp.com/',
  handbook_port = process.env.HANDBOOK_PORT || '',
  plugin_url = process.env.PLUGIN_URL || 'https://diagnose-plugin.herokuapp.com/',
  plugin_port = process.env.PLUGIN_PORT || '',
  ed = require('../../utils/crypt/encrypt_data'),
  ro = require('../../mongodb/commands/read_one.js'),
  co = require('../../mongodb/commands/create_one.js'),
  config_db = require('../../mongodb/config_db.js');

// Handles messaging_postbacks events
exports.handlePostback = async function handlePostback(
  sender_psid,
  received_postback
) {
  // Get the payload for the postback
  let payload = received_postback.payload,
    callhome_leaf = await sol.show_one_leaf('CALL_HOME');

  if (payload == 'GET_STARTED') {
    let user = await ro.read_one(config_db.collection.USER, { IS_ACTIVE: 1, PSID: sender_psid }, {});
    console.log(user);
    if (user) {
      get_started_leaf = await sol.show_one_leaf('GET_STARTED');
      get_started_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
      setTimeout(async () => {
        home_leaf = await sol.show_one_leaf('HOME');
        home_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
      }, 1000);
    } else {
      let gfud = require('../../utils/facebook/get_facebook_user_data.js'),
        facebook_user_info = await gfud.get_fb_user_info(sender_psid),
        
        data = {
        FULL_NAME: facebook_user_info.name,
        PROFILE_PIC: facebook_user_info.profile_pic,
        IS_ACTIVE: 1,
        PSID: sender_psid,
        DOB: null,
        GENDER: null,
        EMAIL: null,
        DIAGNOSE: [],
        CREATED_AT: new Date(),
        UPDATED_AT: null,
        UPDATED_BY: null,
      };

      console.log(facebook_user_info);
      await co.create_one(config_db.collection.USER, data);

      get_started_leaf = await sol.show_one_leaf('GET_STARTED');
      get_started_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
      setTimeout(async () => {
        home_leaf = await sol.show_one_leaf('HOME');
        home_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
      }, 1000);
    }
  }
  else if (payload == 'QUICK_DIAGNOSE') {
    quick_diagnose_leaf = await sol.show_one_leaf(payload);
    quick_diagnose_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/diagnose_heart_prob_form?data=" + ed.encrypt_data(sender_psid.toString());
    quick_diagnose_leaf[0].VALUE.attachment.payload.elements[1].buttons[0].url = plugin_url + plugin_port + "/diagnose_form?data=" + ed.encrypt_data(sender_psid.toString());
    quick_diagnose_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 1000);
    });

    setTimeout(async () => {
      callhome_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }, 1000);
  }
  else if (payload == 'APPOINTMENT') {
    create_appointment_leaf = await sol.show_one_leaf(payload);
    create_appointment_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/create_appointment_bachmai?data=" + ed.encrypt_data(sender_psid.toString());
    create_appointment_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 1000);
    });
    setTimeout(async () => {
      callhome_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }, 1000);
  }
  else if (payload == 'HISTORY') {
    let user_data = await ro.read_one(config_db.collection.USER, {PSID: sender_psid, IS_ACTIVE: 1}, {});
    let response = {
      text: 'Lịch sử chẩn đoán của bạn: '
    }
    user_data.DIAGNOSE.map( e => {
      response.text += '\n- Ngày chẩn đoán: ' + e.CREATED_DATE.toUTCString() + '\n- Kết quả: ' + e.STATUS 
    })
    await call_send_API.callSendAPI(sender_psid, response);
    setTimeout(async () => {
      callhome_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }, 2000);
  }
  else if (payload == 'WEATHER') {
    weather_leaf = await hwp.handle_weather_payload();
    await call_send_API.callSendAPI(sender_psid, weather_leaf);
    setTimeout(async () => {
      callhome_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }, 1000);
  }
  else if (payload == 'HANDBOOK') {
    handbook_leaf = await sol.show_one_leaf(payload);
    handbook_leaf[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/diagnose_form?data=";
    handbook_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 1000);
    });
    setTimeout(async () => {
      callhome_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }, 1000);
  }
  else if (payload == 'HOME') {
    home_leaf = await sol.show_one_leaf('HOME');
    home_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 1000);
    });
  }
  else if (payload == 'NETWORK') {
    network_leaf = await sol.show_one_leaf('NETWORK');
    network_leaf.map(async (e) => {
      setTimeout(async () => {
        await call_send_API.callSendAPI(sender_psid, e.VALUE);
      }, 1000);
    });
  }
  else {
    leaf = await sol.show_one_leaf(payload);
    if (leaf) {
      leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
      setTimeout(async () => {
        callhome_leaf.map(async (e) => {
          setTimeout(async () => {
            await call_send_API.callSendAPI(sender_psid, e.VALUE);
          }, 1000);
        });
      }, 1000);
    } else {
      default_leaf = await sol.show_one_leaf('DEFAULT');
      default_leaf.map(async (e) => {
        setTimeout(async () => {
          await call_send_API.callSendAPI(sender_psid, e.VALUE);
        }, 1000);
      });
    }
  }

}
