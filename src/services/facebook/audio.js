var call_send_API = require('./call_send_API');

exports.audio = async function audio(sender_psid) {
  response = {
    "text": "You just sent a recording."
  }
  await call_send_API.callSendAPI(sender_psid, response);
}