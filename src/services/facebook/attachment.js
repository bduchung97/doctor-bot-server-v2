exports.attachment = async function attachment(sender_psid, attachment) {
  if (attachment.type == 'location') {
    let c = require('./coordinates.js');
    await c.coordinates(sender_psid, attachment);
  } else if (attachment.type == 'video') {
    let v = require('./video.js');
    await v.video(sender_psid);
  } else if (attachment.type == 'audio') {
    let a = require('./audio.js');
    await a.audio(sender_psid);
  } else if (attachment.type == 'image') {
    let i = require('./image.js');
    await i.image(sender_psid, attachment);
  } else {
    let ua = require('./undefined_attachment.js');
    await ua.undefined_attachment(sender_psid);
  }
}