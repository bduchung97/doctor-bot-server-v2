const request = require('request'),
  PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || "EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD";

// Sends response messages via the Send API
exports.callSendAPI = async function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }
  try {
    // Send the HTTP request to the Messenger Platform
    await request({
      "uri": "https://graph.facebook.com/v2.6/me/messages",
      "qs": {
        "access_token": PAGE_ACCESS_TOKEN
      },
      "method": "POST",
      "json": request_body
    });
  } catch (error) {
    console.log(error)
  }

}
