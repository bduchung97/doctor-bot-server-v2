var call_send_API = require('./call_send_API');

exports.undefined_attachment = async function undefined_attachment(sender_psid) {
  response = {
    "text": "You just sent a undefined attachment."
  }
  await call_send_API.callSendAPI(sender_psid, response);
}