const config_db = require('../../mongodb/config_db.js'),
ro = require('../../mongodb/commands/read_one.js');

exports.post_show_history = async function post_show_history(req, res) {
	try {
		let PSID = req.body.PSID,

		if (PSID) {
			let result = await ro.read_one(config_db.collection.USER, { PSID: PSID, IS_ACTIVE: 1 }, {});
			console.log(result)
			console.log('post_show_history successfully');
			let csa = require('../facebook/call_send_API.js'),
				sol = require('../../mongodb/leaf/show_one_leaf.js'),
				response = {
					'text': 'Lịch sử chẩn đoán của bạn như sau: '
				},
				default_leaf = await sol.show_one_leaf('DEFAULT');
			await csa.callSendAPI(PSID, response);
			let history_date = {
				'text': 'Ngày chẩn đoán: ' + result.DIAGNOSE.get(0).CREATED_DATE.toUTCString()
			},
				history_status = {
					'text': 'Kết quả: ' + result.DIAGNOSE.get(0).STATUS
				};

			await csa.callSendAPI(PSID, history_date);
			await csa.callSendAPI(PSID, history_status);
			console.log(default_leaf[0].VALUE)
			setTimeout(async () => {
				await csa.callSendAPI(PSID, default_leaf[0].VALUE);
			}, 1000)
			res.status(200).send({ msg_code: '001' });
		} else {
			console.log('Input is invalid!')
			res.status(400).send({ msg_code: '004' });
		}
	} catch (error) {
		console.log(error);
		res.status(500).send({ msg_Code: '500' });
	}
}