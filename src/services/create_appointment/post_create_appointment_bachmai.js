const config_db = require('../../mongodb/config_db.js'),
  co = require('../../mongodb/commands/create_one.js'),
  uo = require('../../mongodb/commands/update_one.js');

exports.post_create_appointment_bachmai = async function post_create_appointment_bachmai(req, res) {
  try {
    let fullname = req.body.FULLNAME,
      dob = req.body.DOB,
      gender = req.body.GENDER,
      email = req.body.EMAIL,
      phone_number = req.body.PHONE_NUMBER,
      shift = req.body.SHIFT,
      day_appointment = req.body.DAY_APPOINTMENT,
      address = req.body.ADDRESS,
      PSID = req.body.PSID;

    if (fullname && dob && gender && email && phone_number && shift && day_appointment && address && PSID) {
      let data = {
        FULLNAME: fullname,
        DOB: dob,
        GENDER: gender,
        EMAIL: email,
        PHONE_NUMBER: phone_number,
        SHIFT: shift,
        DAY_APPOINTMENT: day_appointment,
        ADDRESS: address,
        PSID: PSID
      }

      await co.create_one(config_db.collection.APPOINTMENT_DATA, data);
      
      await uo.update_one(config_db.collection.USER ,{ PSID: PSID }, {
        $set: {
          FULLNAME: fullname,
          PHONE_NUMBER: phone_number,
          GENDER: gender,
          DATE_OF_BIRTH: dob,
          EMAIL: email,
          UPDATED_AT: new Date()
        }
      })
      console.log('post_create_appointment successfully');
      let csa = require('../../services/facebook/call_send_API'),
        sol = require('../../mongodb/leaf/show_one_leaf'),
        response = {
          'text': 'Bạn vừa đăng ký thành công lịch khám vào ngày: ' + day_appointment + '\nChúng tôi sẽ liên hệ với bạn sau.'
        },
        default_leaf = await sol.show_one_leaf('DEFAULT');
        await csa.callSendAPI(PSID, response);

        console.log(default_leaf[0].VALUE)
        setTimeout(async () => {
          await csa.callSendAPI(PSID, default_leaf[0].VALUE);
        }, 1000)
      res.status(200).send({ msg_code: '001' });
    } else {
      console.log('Input is invalid!')
      res.status(400).send({ msg_code: '004' });
    }
  } catch (error) {
    console.log(error);
    console.log('Server Error')
    res.status(500).send({ msg_code: '005' });
  }
}
