const db = require('../connect_db'),
  config = require('../config_db');

exports.show_one_node = async function show_one_node(function_id) {
  let result = await db.get().collection(config.collection.NODE).findOne({ FUNCTION_ID: function_id });
  return result;
}
