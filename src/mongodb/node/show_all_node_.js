const db = require('../connect_db'),
  config = require('../config_db');

exports.show_all_node_ = async function show_all_node_() {
  let result = await db.get().collection(config.collection.NODE).find({
    PARENT_FUNCTION__ID: { $ne: 'ROOT' }
  }).toArray();

  return result;
}
