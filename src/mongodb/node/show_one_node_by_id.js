const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.show_one_node_by_id = async function show_one_node_by_id(node_id) {
  let result = await db.get().collection(config.collection.NODE).findOne({ _id: ObjectId(node_id) });
  return result;
}
