const db = require('../connect_db'),
  config = require('../config_db'),
  ObjectId = require('mongodb').ObjectID;

exports.update_node = async function update_node(node_id, data) {
  let result = await db.get().collection(config.collection.NODE).updateOne({ _id: ObjectId(node_id) }, { $set: data });
  return result;
}

