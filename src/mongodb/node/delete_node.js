const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db.js');

exports.delete_node = async function delete_node(node_id) {
  let result = await db.get().collection(config.collection.NODE).deleteOne({ _id: ObjectId(node_id) });
  return result;
}
