const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.create_node = async function create_node(
  function_id,
  parent_function_id,
  created_by,
  description,
) {
  let data = {
    FUNCTION_ID: function_id,
    PARENT_FUNCTION__ID: parent_function_id,
    DESCRIPTION: description,
    CREATED_AT: new Date(),
    CREATED_BY: ObjectId(created_by),
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  let result = await db.get().collection(config.collection.NODE).insertOne(data);
  return result.insertedId;
}
