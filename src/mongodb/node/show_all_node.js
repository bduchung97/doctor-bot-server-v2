const db = require('../connect_db'),
  config = require('../config_db');

exports.show_all_node = async function show_all_node() {
  let result = await db.get().collection(config.collection.NODE).find({
    FUNCTION_ID: { $ne: 'PERSISTENT_MENU' }
  }).toArray();

  return result;
}
