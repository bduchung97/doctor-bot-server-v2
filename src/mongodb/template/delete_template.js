const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db.js');

exports.delete_template = async function delete_template(template_id) {
  return await db.get().collection(config.collection.TEMPLATE).deleteOne({ _id: ObjectId(template_id) });
}
