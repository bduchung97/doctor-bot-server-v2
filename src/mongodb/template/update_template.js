const db = require('../connect_db'),
  config = require('../config_db'),
  mongodb = require('mongodb'),
  ObjectId = require('mongodb').ObjectID;

exports.update_template = async function update_template(template_id, data) {
  return await db.get().collection(config.collection.TEMPLATE).updateOne({ _id: ObjectId(template_id) }, { $set: data });
}
