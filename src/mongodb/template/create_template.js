const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.create_template = async function create_template(
  node_ID,
  value,
  created_by,
  type,
) {
  let ct = require('../../utils/validate_leaf/count_templates.js'),
    count = await ct.count_templates(node_ID),
    data = {
      NODE__ID: ObjectId(node_ID),
      VALUE: value,
      TYPE: type,
      ORDER: count,
      CREATED_DATE: new Date(),
      CREATED_BY: ObjectId(created_by),
      UPDATED_AT: null,
      UPDATED_BY: null
    },
    result = await db.get().collection(config.collection.TEMPLATE).insertOne(data);
  return result.insertedId;
}
