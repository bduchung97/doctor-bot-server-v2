const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.show_templates_by_order = async function show_templates_by_order(node_ID) {
  return await db.get().collection(config.collection.TEMPLATE).find({ NODE__ID: ObjectId(node_ID) }).sort({ ORDER: 1 }).toArray();
}
