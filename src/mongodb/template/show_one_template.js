const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.show_one_template = function show_one_template(template_id) {
  return await db.get().collection(config.collection.TEMPLATE).findOne({ _id: ObjectId(template_id) });
}
