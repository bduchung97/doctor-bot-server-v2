const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db.js');

exports.delete_many_templates = async function delete_many_templates(node_id) {
  return await db.get().collection(config.collection.TEMPLATE).deleteMany({ NODE__ID: ObjectId(node_id) });
}
