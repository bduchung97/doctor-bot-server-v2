const
  config = require('../config_db.js'),
  hp = require('../../utils/crypt/hash_password.js'),
  ObjectId = require('mongodb').ObjectID,
  db = require('../connect_db.js');

exports.create_user = async function create_user(
  full_name,
	dob,
	gender,
	PSID,
	email,
	diagnose
) {
  let data = {
    FULL_NAME: full_name,
		IS_ACTIVE: 1,
		PSID: PSID,
		DOB: dob,
		GENDER: gender,
		EMAIL: email,
		DIAGNOSE: diagnose,
    CREATED_AT: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null,
  }
  let result = await db.get().collection(config.collection.ADMIN).insertOne(data);
  return result.insertedId;
}
