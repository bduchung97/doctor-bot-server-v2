const MongoClient = require('mongodb').MongoClient,
  config = require('./config_db');

let mgdb;

async function connect() {
  const client = await MongoClient.connect(
    config.url,
    { useNewUrlParser: true, poolSize: 80 }
  )
  mgdb = client.db(config.dbName);
  return mgdb;
}

function get() {
  return mgdb;
}

function close() {
  mgdb.close();
}

module.exports = {
  connect,
  get,
  close
};
