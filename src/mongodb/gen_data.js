const
  PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || "EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD",
  request = require('request'),
  dac = require('./drop_all_collections'),
  ca = require('./admin/create_admin'),
  cl = require('../mongodb/leaf/create_leaf.js'),
  ct = require('../mongodb/template/create_template'),
  cmap = require('./admin_permission/create_multiple_admin_permissions'),
  current_date = new Date(),
  admin_permission = require('./mockup_data/permission'),
  persistent_menu = require('./mockup_data/persistent_menu'),
  login_form_value = require('./mockup_data/login_form'),
  home_value = require('./mockup_data/home_leaf'),
  cqrr = require('../mongodb/quick_replies_ref/create_quick_replies_ref.js'),
  call_home_value = require('./mockup_data/call_home'),
  weather_value = require('./mockup_data/weather_leaf'),
  default_leaf_value = require('./mockup_data/default_leaf'),
  quick_diagnose = require('./mockup_data/quick_diagnose'),
  create_appointment = require('./mockup_data/create_appointment.js'),
  active_ai_value = require('./mockup_data/active_ai'),
  product_n_services = require('./mockup_data/product_n_services'),
  products = require('./mockup_data/products'),
  products2 = require('./mockup_data/products2'),
  services = require('./mockup_data/services'),
  network = require('./mockup_data/network'),
  insurance_network = require('./mockup_data/insurance_network'),
  iatfef = require('../mongodb/ai_trainning/import_ai_trainning_from_excel_file'),
  HANDBOOK_value = require('./mockup_data/handbook');

exports.gen_data = async function gen_data() {
  // Xoá all collections
  await dac.drop_all_collections();

  // Tạo admin ROOT
  var root_id = await ca.create_admin(null, 'ROOT', 'ROOT', null, null, null, null)
  console.log('create admin ROOT successfully!')

  // Tạo quyền
  admin_permission.map((e, i) => {
    Object.assign(e, {
      CREATED_AT: current_date,
      CREATED_BY: root_id
    })
  })
  await cmap.create_multiple_admin_permissions(admin_permission);
  console.log('create admin_permission successfully!')

  // Tạo admin con
  await ca.create_admin(
    'Nguyễn Quang Anh',
    'quanh_nguyen',
    'root',
    'quanh_nguyen@outlook.com',
    '0915781197',
    ['ADMN', 'USER', 'GROU', 'DATA', 'AITR', 'HAND', 'NOTI', 'SCHE', 'STAT'],
    root_id
  )
  console.log('create quanh_nguyen successfully!')

  // Khởi tạo bot facebook
  let gt = {
    "get_started": { "payload": "GET_STARTED" }
  }
  await request({
    url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + PAGE_ACCESS_TOKEN,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    form: gt
  });
  console.log('create get_started successfully!')

  // Tạo pesistent menu
  let pm_leaf = await cl.create_leaf('PERSISTENT_MENU', 'ROOT', persistent_menu, root_id, 'Menu nhanh', 'PERSISTENT_MENU');
  if (pm_leaf) {
    await request({
      url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + PAGE_ACCESS_TOKEN,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      form: persistent_menu
    });
    console.log('create PERSISTENT_MENU successfully!')
  } else {
    console.log('create PERSISTENT_MENU unsuccessfully!')
  }

  // Tạo lá bắt đầu
  let get_started_value = {
    "text": "Xin chào bạn đến với Doctor Bot"
  }
  await cl.create_leaf('GET_STARTED', 'ROOT', get_started_value, root_id, 'Câu chào', 'TEXT');
  console.log('create get_started_leaf successfully!')

  // Lá chào
  let greetings_value = {
    "text": ""
  }
  await cl.create_leaf('GREETINGS', 'ROOT', greetings_value, root_id, 'Câu chào', 'TEXT');
  console.log('create GREETINGS successfully!')

  // Lá home
  await cl.create_leaf('HOME', 'GET_STARTED', home_value, root_id, 'Trang chủ', 'GENERIC');
  console.log('create home_value successfully');

  

  // Lá default
  await cl.create_leaf('DEFAULT', 'ROOT', default_leaf_value, root_id, 'Trả lời nhanh', 'QUICK_REPLIES');
  console.log('create DEFAULT successfully');
  await cqrr.create_quick_replies_ref(root_id);
  console.log('create create_quick_replies_ref successfully');

  //  Lá call home
  await cl.create_leaf('CALL_HOME', 'ROOT', call_home_value, root_id, 'TRỞ VỀ TRANG CHỦ', 'QUICK_REPLIES');
  console.log('create CALL_HOME successfully');

  // Lá thời tiết
  await cl.create_leaf('WEATHER', 'HOME', weather_value, root_id, 'THỜI TIẾT', 'GENERIC')
  console.log('create WEATHER successfully');

  // Lá chẩn đoán nhanh
  await cl.create_leaf('QUICK_DIAGNOSE', 'HOME', quick_diagnose, root_id, 'CHẨN ĐOÁN NHANH', 'GENERIC');
  console.log('create QUICK_DIAGNOSE successfully');

  // Lá đặt/hẹn lịch hẹn
  await cl.create_leaf('APPOINTMENT', 'HOME', create_appointment, root_id, 'ĐẶT/HẸN LỊCH KHÁM', 'GENERIC');
  console.log('create APPOINTMENT successfully');
}
