const db = require('../connect_db'),
  config = require('../config_db.js');

exports.insert_many = async function insert_many(data) {
  return await db.get().collection(config.collection.QUICK_REPLIES_REF).insertMany(data);
}
