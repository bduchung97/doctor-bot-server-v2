const db = require('../connect_db'),
  config = require('../config_db.js');

exports.delete_all_documents = async function delete_all_documents() {
  return await db.get().collection(config.collection.QUICK_REPLIES_REF).deleteMany({});
}
