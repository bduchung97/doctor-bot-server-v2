const ObjectId = require('mongodb').ObjectID;
exports.create_quick_replies_ref = async function create_quick_replies_ref(create_by, callback) {
  let input = [],
    sol = require('../leaf/show_one_leaf.js');
  var leaf_data = await sol.show_one_leaf('DEFAULT');
  if (leaf_data) {
    let templates = leaf_data[0].VALUE.quick_replies;
    templates.map(element => {
      input.push({
        TITLE: element.title,
        PAYLOAD: element.payload
      });
    });
    input.map(e => {
      e.CREATED_AT = new Date(),
        e.CREATED_BY = ObjectId(create_by)
    });

    let im = require('./insert_many.js');
    return await im.insert_many(input);
  } else {
    return null;
  }
}
