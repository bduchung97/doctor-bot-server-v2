const db = require('../connect_db'),
  config = require('../config_db');

exports.show_one_quick_replies_ref = async function show_one_quick_replies_ref(title) {
  return await db.get().collection(config.collection.QUICK_REPLIES_REF).findOne({ TITLE: title })
}
