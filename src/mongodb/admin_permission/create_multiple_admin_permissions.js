const db = require('../connect_db'),
  config = require('../config_db');

exports.create_multiple_admin_permissions = async function create_multiple_admin_permissions(
  data,
) {
  let result = await db.get().collection(config.collection.ADMIN_PERMISSION).insertMany(data);
  return result.insertedIds;
}
