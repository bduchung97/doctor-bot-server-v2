const db = require('../connect_db'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config_db');

exports.create_admin_permission = async function create_admin_permission(
  code,
  description,
  admin_id,
) {
  let data = {
    CODE: code,
    DESCRIPTION: description,
    CREATED_AT: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null,
    CREATED_BY: ObjectId(admin_id)
  }
  let result = await db.get().collection(config.collection.ADMIN_PERMISSION).insertOne(data);
  return result.insertedId;
}
