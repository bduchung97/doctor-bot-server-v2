const
  db = require('../connect_db.js');

exports.create_one = async function create_one(
	collection,
	data
) {
  return await db.get().collection(collection).insertOne(data);
}
