const
  db = require('../connect_db.js');

exports.update_one = async function update_one(
	collection,
	filter,
	update
) {
  return await db.get().collection(collection).updateOne(filter, update);
}
