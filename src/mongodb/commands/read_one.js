const
  db = require('../connect_db.js');

exports.read_one = async function read_one(
	collection,
	filter,
	projection
) {
  return await db.get().collection(collection).findOne(filter, projection);
}
