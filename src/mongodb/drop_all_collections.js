const db = require('./connect_db');

exports.drop_all_collections = async function drop_all_collections() {
  try {
    const collections = await db.get().listCollections().toArray();
    collections.map((e) => {
      db.get().collection(e.name).drop();
    })
    console.log('Dropp all collections successfully');
  } catch (error) {
    console.error(error);
    console.log('Drop all collections unsuccessfully!')
  }
}
