exports.create_leaf = async function create_leaf(function_id, parent_function_id, value, created_by, description, type, callback) {
  let show_one_node = require('../node/show_one_node.js'),
    create_template = require('../template/create_template.js'),
    node_data = await show_one_node.show_one_node(function_id);
  if (node_data) {
    return null;
  } else {
    let create_node = require('../node/create_node.js'),
      node_id = await create_node.create_node(function_id, parent_function_id, created_by, description);
    await create_template.create_template(node_id, value, created_by, type);
    return node_id;
  }
}
