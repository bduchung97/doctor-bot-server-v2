exports.show_one_leaf = async function show_one_leaf(function_id) {
  let son = require('../node/show_one_node.js'),
    stbo = require('../template/show_templates_by_order.js'),
    node_data = await son.show_one_node(function_id);
  if (node_data) {
    let node_ID = node_data._id;
    return await stbo.show_templates_by_order(node_ID);
  } else {
    return null;
  }
}
