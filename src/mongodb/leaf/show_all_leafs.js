let db = require('../connect_db'),
  config = require('../config_db'), ;

exports.show_all_leafs = async function show_all_leafs() {
  return await db.get().collection(config.collection.NODE).aggregate([
    {
      $lookup:
      {
        from: config.collection.TEMPLATE,
        localField: '_id',
        foreignField: 'NODE__ID',
        as: 'templates'
      }
    }, {
      $match:
      {
        PARENT_FUNCTION__ID: { $ne: 'ROOT' }
      }
    }
  ]).toArray();
}
