const network = {
  "text": "Please share your location!",
  "quick_replies": [
    {
      "content_type": "location",
      "payload": "NETWORK"
    }
  ]
}

module.exports = network;
