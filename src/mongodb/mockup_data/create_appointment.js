const create_appointment = {
	"attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Viện tim mạch - Bệnh viện Bạch Mai",
          "image_url": "https://hellobacsi.com/wp-content/uploads/2018/07/kh%C3%A1m-b%E1%BB%87nh-%E1%BB%9F-b%E1%BB%87nh-vi%E1%BB%87n-B%E1%BA%A1ch-Mai-1-1024x400.jpg",
          "subtitle": "78 đường Giải Phóng -Đống Đa - Hà Nội ",
          "buttons": [
            {
              "title": "ĐẶT LỊCH",
              "type": "web_url",
              "url": null,
              "webview_height_ratio": "tall"
            }
          ]
				}
      ]
    }
  }
}

module.exports = create_appointment;