const permission = [{
  CODE: 'ADMN',
  DESCRIPTION: 'Quản trị admin.',
  UPDATED_AT: null,
  UPDATED_BY: null,
}, {
  CODE: 'USER',
  DESCRIPTION: 'Quản trị người dùng.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'GROU',
  DESCRIPTION: 'Quản trị nhóm.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'DATA',
  DESCRIPTION: 'Quản trị dữ liệu cây chatbot.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'AITR',
  DESCRIPTION: 'Quản trị dữ liệu trainning AI.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'HAND',
  DESCRIPTION: 'Quản trị sổ tay tư vấn.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'NOTI',
  DESCRIPTION: 'Quản trị thông báo.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'SCHE',
  DESCRIPTION: 'Quản trị lập lịch trainning chatbot.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}, {
  CODE: 'STAT',
  DESCRIPTION: 'Quản trị thống kê.',

  UPDATED_AT: null,
  UPDATED_BY: null,

}];

module.exports = permission;