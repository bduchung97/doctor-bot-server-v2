const product_n_services = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Products",
          "image_url": "https://i.imgur.com/CrEbgIB.png",
          "subtitle": "Products",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "postback",
              "payload": "PRODUCTS"
            }
          ]
        },
        {
          "title": "Services",
          "image_url": "https://www.manulife.com.vn/vi/dich-vu/_jcr_content/root/responsivegrid_641029165/responsivegrid/responsivegrid/image.coreimg.jpeg/1562559795575.jpeg",
          "subtitle": "Services",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "postback",
              "payload": "SERVICES"
            }
          ]
        }
      ]
    }
  }
}

module.exports = product_n_services;