const products = {
  "text": "We help you protect your loved ones, grow your wealth, improve your health – and build your future. And our goal is to make it fast and easy to do all those things, so you can spend time on the people and things that really matter."
}

module.exports = products;
