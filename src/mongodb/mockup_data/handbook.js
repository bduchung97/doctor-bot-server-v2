const handbook = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Handbook",
          "image_url": "https://i.imgur.com/CcUjisR.jpg",
          "subtitle": "Handbook.",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "web_url",
              "url": null,
              "webview_height_ratio": "tall"
            }
          ]
        }
      ]
    }
  }
}
module.exports = handbook;
