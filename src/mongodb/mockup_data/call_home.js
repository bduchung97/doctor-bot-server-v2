const call_home = {
  "text": "To display all, please press DoctorBot",
  "quick_replies": [
    {
      "content_type": "text",
      "image_url": "https://i.imgur.com/eveSnfv.jpg",
      "title": "DoctorBot",
      "payload": "HOME"
    }
  ]
}

module.exports = call_home;
