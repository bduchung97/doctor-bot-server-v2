const insurance_network = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Insurance network",
          "image_url": "https://www.manulife.com.vn/vi/ve-chung-toi/manulife-financial/_jcr_content/root/responsivegrid_641029165/image.coreimg.jpeg/1562554091136.jpeg",
          "subtitle": "Insurance network",
          "buttons": [
            {
              "title": "FIND",
              "type": "postback",
              "payload": "NETWORK"
            },
          ]
        },
      ]
    }
  }
}

module.exports = insurance_network;