const quick_diagnose = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Kiểm tra bệnh tim mạch vành",
          "image_url": "https://www.vyvanse.com/Content/images/adult/hero/hero-4.jpg",
          "subtitle": "Kiểm tra bệnh tim mạch vành",
          "buttons": [
            {
              "title": "TIẾN HÀNH KIỂM TRA",
              "type": "web_url",
              "url": null,
              "webview_height_ratio": "tall"
            }
          ]
				},
				{
          "title": "Chẩn đoán nhanh",
          "image_url": "https://benhtimmach.info.vn/wp-content/uploads/2015/04/Heart-Disease-1-500x332.jpg",
          "subtitle": "Chẩn đoán nhanh",
          "buttons": [
            {
              "title": "CHẨN ĐOÁN",
              "type": "web_url",
              "url": null,
              "webview_height_ratio": "tall"
            }
          ]
				}
      ]
    }
  }
}
module.exports = quick_diagnose;
