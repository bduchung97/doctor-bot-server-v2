const product_n_services = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Products",
          "image_url": "https://i.imgur.com/hPgKJeC.png",
          "subtitle": "Products",
          "buttons": [
            {
              "title": "LIFE",
              "type": "postback",
              "payload": "PRODUCTS_LIFE"
            },
            {
              "title": "HEALTH",
              "type": "postback",
              "payload": "PRODUCTS_HEALTH"
            },
          ]
        },
        {
          "title": "Products",
          "image_url": "http://www.vivateachers.org/wp-content/uploads/2018/08/student-education-750x460.jpg",
          "subtitle": "Products",
          "buttons": [
            {
              "title": "EDUCATION",
              "type": "postback",
              "payload": "PRODUCTS_EDUCATION"
            },
            {
              "title": "RETIREMENT",
              "type": "postback",
              "payload": "PRODUCTS_RETIREMENT"
            },
          ]
        },
      ]
    }
  }
}

module.exports = product_n_services;