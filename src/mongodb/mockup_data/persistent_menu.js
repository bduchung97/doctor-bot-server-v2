const persistent_menu_value = {
  "persistent_menu": [
    {
      "locale": "default",
      "composer_input_disabled": false,
      "call_to_actions": [
        {
          "title": "Activate chat with AI",
          "type": "postback",
          "payload": "ACTIVATE_AI_CHAT"
        },
        {
          "title": "Weather",
          "type": "postback",
          "payload": "WEATHER"
        }
      ]
    }
  ]
}

module.exports = persistent_menu_value;