const home_leaf = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Chẩn đoán tim mạch",
          "image_url": "https://benhtimmach.info.vn/wp-content/uploads/2015/04/Heart-Disease-1-500x332.jpg",
          "subtitle": "Chẩn đoán về vấn đề tim mạch vành",
          "buttons": [
            {
              "title": "CHI TIẾT",
              "type": "postback",
              "payload": "QUICK_DIAGNOSE"
            }
          ]
        },
        {
          "title": "Lịch sử chẩn đoán",
          "image_url": "https://parkinsonviewpoint.com/wp-content/uploads/2019/07/how-to-diagnose-parkinson.jpg",
          "subtitle": "Lịch sử chẩn đoán của bản thân",
          "buttons": [
            {
              "title": "XEM",
              "type": "postback",
              "payload": "HISTORY"
            }
          ]
        },
        {
          "title": "Đặt/hẹn lịch khám",
          "image_url": "https://vinmec-prod.s3.amazonaws.com/images/20181211_104609_318316_Vinmec.max-800x800.jpg",
          "subtitle": "Đặt/hẹn lịch khám tại những bệnh viện liên kết với chúng tôi",
          "buttons": [
            {
              "title": "CHI TIẾT",
              "type": "postback",
              "payload": "APPOINTMENT"
            }
          ]
        },
        {
          "title": "Weather",
          "image_url": "https://i.imgur.com/mS4ypEz.png",
          "subtitle": "Weather forecast",
          "buttons": [
            {
              "title": "WEATHER",
              "type": "postback",
              "payload": "WEATHER"
            }
          ]
        }
      ]
    }
  }
}

module.exports = home_leaf;