const login_form = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Login",
          "image_url": "https://www.bajajallianz.com/Corp/blog/amp/img/critical-illness-insurance-plans.jpg",
          "subtitle": "Press here to login.",
          "buttons": [
            {
              "title": "LOGIN",
              "type": "web_url",
              "url": null,
              "webview_height_ratio": "tall"
            }
          ]
        }
      ]
    }
  }
}

module.exports = login_form;