const services = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Insurance contract",
          "image_url": "http://sildeal.vn/wp-content/uploads/2019/02/Every-Task-a-Customer-Service-Rep-Must-Do-opengraph.png",
          "subtitle": "Query information",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "postback",
              "payload": "SERVICES_QUERY_INSURANCE_CONTRACT"
            }
          ]
        },
        {
          "title": "Insurance payment",
          "image_url": "http://sildeal.vn/wp-content/uploads/2019/02/Every-Task-a-Customer-Service-Rep-Must-Do-opengraph.png",
          "subtitle": "Insurance online payment",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "postback",
              "payload": "SERVICES_QUERY_INSURANCE_PAYMENT"
            }
          ]
        },
        {
          "title": "Insurance compensation",
          "image_url": "http://sildeal.vn/wp-content/uploads/2019/02/Every-Task-a-Customer-Service-Rep-Must-Do-opengraph.png",
          "subtitle": "Insurance compensation",
          "buttons": [
            {
              "title": "DETAILS",
              "type": "postback",
              "payload": "SERVICES_QUERY_INSURANCE_COMPENSATION"
            }
          ]
        },
      ]
    }
  }
}

module.exports = services;