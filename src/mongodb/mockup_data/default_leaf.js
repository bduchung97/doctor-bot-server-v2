const default_leaf = {
  "text": "There are some suggestions for you.",
  "quick_replies": [
    {
      "content_type": "text",
      "title": "DoctorBot",
      "image_url": "https://i.imgur.com/eveSnfv.jpg",
      "payload": "HOME"
    }
  ]
}

module.exports = default_leaf;
