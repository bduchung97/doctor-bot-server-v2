const weather_leaf = {
  "attachment": {
    "type": "template",
    "payload": {
      "template_type": "generic",
      "elements": [
        {
          "title": "Weather",
          "subtitle": null,
          "image_url": "https://i.imgur.com/mS4ypEz.png",
          "default_action": {
            "type": "web_url",
            "url": "https://www.msn.com/vi-vn/weather",
            "webview_height_ratio": "tall"
          }
        }
      ]
    }
  }
}
module.exports = weather_leaf;
