exports.import_ai_trainning_from_excel_file = async function import_ai_trainning_from_excel_file(file, admin_id, callback) {
  let rfe = require('../../utils/file/read_file_excel'),
    data = await rfe.read_file_excel(file),
    hv = require('../../utils/handle_vie/convert_vi');
  data.map(e => {
    e.VALUE = hv.change_alias(e.VALUE);
    e = Object.assign(e, { IS_ACTIVE: 1, CREATED_BY: admin_id, CREATED_AT: new Date(), UPDATED_AT: null, UPDATED_BY: null });
  })
  var cmat = require('./create_multiple_ai_trainning');
  return await cmat.create_multiple_ai_trainning(data);
}

