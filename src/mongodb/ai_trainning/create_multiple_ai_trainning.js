const db = require('../connect_db'),
  config = require('../config_db');

exports.create_multiple_ai_trainning = async function create_multiple_ai_trainning(data, callback) {
  let result = await db.get().collection(config.collection.AI_TRAINNING).insertMany(data);
  return result.insertedIds;
}

