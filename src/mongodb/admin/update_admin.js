const config = require('../config_db.js'),
  db = require('../connect_db.js');

exports.update_admin = async function update_admin(filter, update) {
  let result = await db.get().collection(config.collection.ADMIN).findOneAndUpdate(filter, update);
  return result;
}
