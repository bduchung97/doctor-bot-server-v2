const config = require('../config_db.js'),
  db = require('../connect_db.js');

exports.show_admin = async function show_admin(filter, projection) {
  let result = await db.get().collection(config.collection.ADMIN).findOne(filter, projection);
  return result;
}
