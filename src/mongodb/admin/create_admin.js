const
  config = require('../config_db.js'),
  hp = require('../../utils/crypt/hash_password.js'),
  ObjectId = require('mongodb').ObjectID,
  db = require('../connect_db.js');

exports.create_admin = async function create_admin(
  full_name,
  username,
  password,
  email,
  phone_number,
  permission,
  admin_id,
) {
  let data = {
    FULL_NAME: full_name,
    IS_ACTIVE: 1,
    USERNAME: username,
    PASSWORD: hp.hash_password(password),
    PHONE_NUMBER: phone_number,
    EMAIL: email,
    PERMISSION: permission,
    ADMIN_SESSION: [],
    LAST_LOGIN: null,
    CREATED_AT: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null,
    CREATED_BY: ObjectId(admin_id),
  }
  let result = await db.get().collection(config.collection.ADMIN).insertOne(data);
  return result.insertedId;
}
