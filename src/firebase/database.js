const admin = require("firebase-admin");
const serviceAccount = require('./chatbot-7d699-firebase-adminsdk-l4gjn-ddf5724e9f.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://chatbot-7d699.firebaseio.com"
});

const adb = admin.database();

module.exports = adb
