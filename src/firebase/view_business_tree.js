let adb = require('./database')

exports.view_business_tree = function view_business_tree(callback) {
  let ref = adb.ref("Business_Tree/Business")

  ref.orderByValue().once("value", function (snapshot) {
    snapshot.map(function (childSnapshot) {
      let childData = childSnapshot.val();
      callback(null, childData)
    });
  });
}