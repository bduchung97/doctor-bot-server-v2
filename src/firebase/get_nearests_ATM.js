let adb = require('./database')

exports.get_nearests_ATM = function get_nearests_ATM(level1long, callback) {
  // let ref = adb.ref("ATM/" + level1long);

  let ref = adb.ref("ATM/Hà Nội"),
    result = [];
  ref.orderByValue().once("value", function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
      let childData = childSnapshot.val();
      let lat = parseFloat(childData.Latitude)
      let lon = parseFloat(childData.Longitude)
      result.push({
        address: childData.Address,
        latitude: lat,
        longitude: lon
      });
    });
    callback(null, result);
    // console.log(result);
  });
}

