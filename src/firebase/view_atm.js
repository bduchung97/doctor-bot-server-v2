let adb = require('./database')

exports.view_atm = function view_atm(callback) {
  let ref = adb.ref("ATM/Hà Nội")
  let childData = []
  ref.orderByValue().on("value", function(snapshot) {
    console.log(snapshot.key)
    snapshot.map(function(childSnapshot) {
      childData.push(childSnapshot.val())
    });
    callback(null, childData)
  });
}